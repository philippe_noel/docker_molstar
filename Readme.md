# Docker Image for Mol*

This is the recipe for a Docker image for the [molstar](https://github.com/molstar/molstar) viewer.
The image does not use root access. See the Dockerfile for more informations.

## Versions
Two versions of the docker for molstar exist. If you have probblems using molstar with the `molstar-slim` tag, use the `molstar` tag instead. Both tags are the same, only the size of the final image differ.

| Molstar Version | Tag          | Image size |
|-----------------|--------------|------------|
| v0.5.6          | molstar      | 1.34 GB    |
| v0.5.6          | molstar-slim | 586 MB     |

## Usage
For the molstar image
```bash
$ git clone https://gitlab.com/philippe_noel/docker_molstar
$ cd docker_molstar
$ docker build -t molstar:latest . -f Dockerfile
$ docker run -p 3890:3890 molstar:latest
$ firefox localhost:3890
```


For the molstar-slim image
```bash
$ git clone https://gitlab.com/philippe_noel/docker_molstar
$ cd docker_molstar
$ docker build -t molstar-slim:latest . -f Dockerfile.slim
$ docker run -p 3890:3890 molstar-slim:latest
$ firefox localhost:3890
```

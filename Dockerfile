FROM node:stretch
MAINTAINER Philippe Noel <philippe.bs.noel@tutanota.com>

ENV MOLSTAR_VERSION v0.5.6
ENV MOLSTAR_PORT 3890
ENV MOLSTAR_USER classic_user

SHELL ["/bin/bash", "-c"]

RUN adduser --gecos "" --disabled-password "${MOLSTAR_USER}" && \
    git clone https://github.com/molstar/molstar.git /src/molstar && \
    cd /src/molstar && \
    git checkout "${MOLSTAR_VERSION}" && \
    npm install && \
    npm install -g http-server && \
    chown -R "${MOLSTAR_USER}:${MOLSTAR_USER}" /src

WORKDIR /src/molstar

USER "${MOLSTAR_USER}"
RUN NODE_ENV=production && \
    npm run build

WORKDIR /src/molstar/build/viewer/
EXPOSE 3890

ENTRYPOINT http-server . -p ${MOLSTAR_PORT}
